mod forest;
mod simulation;
mod util;

use forest::Forest;
use simulation::Simulation;

use std::env;
use std::path::Path;

fn main() {
    if env::args().len() < 5 {
        println!("Five parameters needed:");
        println!("- Path to the text file");
        println!("- Ignition probability");
        println!("- Growth probability");
        println!("- Num generations");
        println!("Wrong number of parameters :(");
        return;
    }

    // Path to the text file
    let path = env::args().nth(1).unwrap();
    let path = Path::new(&path);
    // Ignition probability
    let ignition = env::args().nth(2).unwrap().parse::<f64>().unwrap();
    // Growth probability
    let growth = env::args().nth(3).unwrap().parse::<f64>().unwrap();
    // Num Generations
    let num_generations = env::args().nth(4).unwrap().parse::<u64>().unwrap();

    let forest = Forest::from_file(&path);
    let mut simulation = Simulation::new(forest, ignition, growth);

    for generation in 0..num_generations {
        println!("Generation:\t{}", generation);
        println!("{}", simulation.forest);
        simulation.next_generation();
        println!("--------------");
    }
}
