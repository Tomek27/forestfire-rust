use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::path::Path;

use forest::{Field, Forest};
use forest::{FIELD_EMPTY_SIGN, FIELD_TREE_SIGN, FIELD_BURNING_SIGN};

/// Loading the init state for the forest
/// using following format:
///
/// * **_ (Space)** Empty space
/// * **T** A tree
/// * **X** A burning tree
///
/// _Example:_
///
/// TT X T
/// TTT  X
/// T T T
pub fn load_text_file(path: &Path) -> Forest {
    let f = File::open(&path).unwrap();
    let file = BufReader::new(&f);

    let mut height = 0usize;
    let mut fields = Vec::<Field>::new();

    for line in file.lines() {
        for c in line.unwrap().chars() {
            match c {
                FIELD_EMPTY_SIGN => {
                    fields.push(Field::Empty);
                }
                FIELD_TREE_SIGN => {
                    fields.push(Field::Tree);
                }
                FIELD_BURNING_SIGN => {
                    fields.push(Field::Burning);
                }
                _ => (),
            }
        }
        height += 1;
    }
    fields.shrink_to_fit();

    Forest {
        width: fields.len() / height,
        height,
        fields,
    }
}
