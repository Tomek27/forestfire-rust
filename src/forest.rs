use std::fmt;
use std::path::Path;

use util::load_text_file;

#[derive(Debug, Clone, Copy)]
pub enum Field {
    Empty,
    Tree,
    Burning,
}

pub const FIELD_EMPTY_SIGN: char = ' ';
pub const FIELD_TREE_SIGN: char = 'T';
pub const FIELD_BURNING_SIGN: char = 'X';

#[derive(Debug, Clone)]
pub struct Forest {
    pub width: usize,
    pub height: usize,
    pub fields: Vec<Field>,
}

impl Forest {
    pub fn from_file(path: &Path) -> Self {
       load_text_file(path)
    }

    /// Returns the number of burning and not burning trees in the neighborhood
    pub fn neighbors_burning_and_not(&self, position: usize) -> (u64, u64) {
        let mut burning = 0;
        let mut not_burning = 0;

        let position_compare = position as isize;
        let forest_width_compare = self.width as isize;
        let forest_height_compare = self.height as isize;

        // +--+-+--+
        // |NW|N|NE|
        // |W |.| E|
        // |SW|S|SE|
        // +--+-+--+
        let mut calc_neighbors = |pos| {
            match self.fields[pos] {
                Field::Empty => (),
                Field::Tree => not_burning += 1,
                Field::Burning => burning += 1,
            };
            (burning, not_burning)
        };

        let num_fields = forest_width_compare * forest_height_compare;
        let mut count_trees = (0u64, 0u64);

        // North West neighbor
        if position_compare - forest_width_compare - 1 >= 0 {
            count_trees = calc_neighbors(position - self.width - 1);
        }

        // North neighbor
        if position_compare - forest_width_compare >= 0 {
            count_trees = calc_neighbors(position - self.width);
        }

        // North East neighbor
        if position_compare - forest_width_compare + 1 >= 0 {
            count_trees = calc_neighbors(position - self.width + 1);
        }

        // West neighbor
        if position_compare - 1 >= 0 {
            count_trees = calc_neighbors(position - 1);
        }

        // East neighbor
        if position_compare + 1 < num_fields {
            count_trees = calc_neighbors(position + 1);
        }

        // South West neighbor
        if position_compare + forest_width_compare - 1 < num_fields {
            count_trees = calc_neighbors(position + self.width - 1);
        }

        // South neighbor
        if position_compare + forest_width_compare < num_fields {
            count_trees = calc_neighbors(position + self.width);
        }

        // South East neighbor
        if position_compare + forest_width_compare + 1 < num_fields {
            count_trees = calc_neighbors(position + self.width + 1);
        }

        count_trees
    }
}

impl fmt::Display for Field {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let c = match self {
            &Field::Empty => FIELD_EMPTY_SIGN,
            &Field::Tree => FIELD_TREE_SIGN,
            &Field::Burning => FIELD_BURNING_SIGN,
        };
        write!(f, "{}", c)
    }
}

impl fmt::Display for Forest {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for h in 0..self.height {
            for w in 0..self.width {
                write!(f, "{}", self.fields[h * self.width + w])?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}
