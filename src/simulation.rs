extern crate rand;

use self::rand::Rng;

use forest::{Field, Forest};

#[derive(Clone, Debug)]
pub struct Simulation {
    pub forest: Forest,
    pub ignition_prob: f64,
    pub growth_prob: f64,
}

impl Simulation {
    pub fn new(forest: Forest, ignition_prob: f64, growth_prob: f64) -> Self {
        Simulation {
            forest,
            ignition_prob,
            growth_prob,
        }
    }

    /// Transform the forest into the next generation, depending on the probabilities
    pub fn next_generation(&mut self) {
        let old_forest = self.forest.clone();

        for pos in 0..old_forest.fields.len() {
            // Get the number of burning and not-burning neighboring trees
            let (burning, not_burning) = self.forest.neighbors_burning_and_not(pos);

            // Define next generation
            match old_forest.fields[pos] {
                // An empty field will grow a new tree with certain probability
                Field::Empty => {
                    if self.should_tree_grow(not_burning) {
                        self.forest.fields[pos] = Field::Tree;
                    } else {
                        self.forest.fields[pos] = Field::Empty;
                    }
                }
                // A tree that is next to a burning tree will turn into a burning tree itself.
                Field::Tree => {
                    if burning > 0 {
                        self.forest.fields[pos] = Field::Burning;
                    // A tree with no burning neighbors will ignite with probability `ignition`
                    // as a result of a lightning strike.
                    } else {
                        if self.should_tree_ignite() {
                            self.forest.fields[pos] = Field::Burning;
                        } else {
                            self.forest.fields[pos] = Field::Tree;
                        }
                    }
                }
                // A burning tree will turn into an empty space
                Field::Burning => {
                    self.forest.fields[pos] = Field::Empty;
                }
            }
        }
    }

    fn should_tree_grow(&self, trees_not_burning: u64) -> bool {
        Simulation::random01() < self.growth_prob * (trees_not_burning + 1) as f64
    }

    fn should_tree_ignite(&self) -> bool {
        Simulation::random01() < self.ignition_prob
    }

    fn random01() -> f64 {
        rand::thread_rng().gen_range(0.0, 1.0)
    }
}
